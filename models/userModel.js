var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var UserSchema = Schema({
    email: String,
    password: String,
    phone: String,
    avatar: String,
    address: String,
    lat: String,
    long: String,
    // idWard: { type: Schema.ObjectId, ref: "role" },
    idRole: { type: Schema.ObjectId, ref: "role" }
});

var userModel = mongoose.model("user", UserSchema);
module.exports = {
    userModel: userModel
}