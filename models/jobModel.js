var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var jobSchema = Schema({
    title: String,
    sex: String,
    idSpecialize: { type: Schema.ObjectId, ref: "specialize" },
    experience: String,
    typeWork: String,
    salary: { from: Number, to: Number },
    term: String,//ky han
    requirement: String,
    benefit: String,
    idClinic: { type: Schema.ObjectId, ref: "clinic" },
    isActive: Boolean,
    status: Boolean,
    address: String,
    idDistrict: { type: Schema.ObjectId, ref: "district" }
});

var jobModel = mongoose.model("job", jobSchema);
module.exports = {
    jobModel: jobModel
}