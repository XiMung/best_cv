var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var productSchema = Schema({
    name: String,
    price: Number,
    money: Number
});

var productModel = mongoose.model("product", productSchema);
module.exports = {
    productModel: productModel
}