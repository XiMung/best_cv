var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var citySchema = Schema({
    name: String
});

var specializeModel = mongoose.model("specialize", citySchema);
module.exports = {
    specializeModel: specializeModel
}