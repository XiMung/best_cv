var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var reviewSchema = Schema({
    comment: String,
    idClinic: { type: Schema.ObjectId, ref: "clinic" }
});

var reviewModel = mongoose.model("review", reviewSchema);
module.exports = {
    reviewModel: reviewModel
}