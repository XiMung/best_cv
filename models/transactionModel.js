var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var transactionSchema = Schema({
    idProduct: { type: Schema.ObjectId, ref: "product" },
    idClinic: { type: Schema.ObjectId, ref: "clinic" },
    createBy: { type: Schema.ObjectId, ref: "user" },
    created: { type: Date, required: true, default: Date.now },
    state: String
});

var transactionModel = mongoose.model("transaction", transactionSchema);
module.exports = {
    transactionModel: transactionModel
}