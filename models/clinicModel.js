var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var clinicSchema = Schema({
    name: String,
    website: String,
    idUser: { type: Schema.ObjectId, ref: "user" },
    video: String,
    coin: Number,
    description: String,
    scale: String,
    image: String
});

var clinicModel = mongoose.model("clinic", clinicSchema);
module.exports = {
    clinicModel: clinicModel
}