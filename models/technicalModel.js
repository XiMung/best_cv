var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var technicalSchema = Schema({
    experience: String,
    idSpecialize: { type: Schema.ObjectId, ref: "specialize" },
    idDoctor: { type: Schema.ObjectId, ref: "doctor" },
    idCv: { type: Schema.ObjectId, ref: "cv" },
    certificate: String,
    sex: String,
    idDistrict: { type: Schema.ObjectId, ref: "district" }
});

var technicalModel = mongoose.model("technical", technicalSchema);
module.exports = {
    technicalModel: technicalModel
}