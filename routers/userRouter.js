var router = require("express").Router();
var { userModel } = require("../models/userModel");
var cons = require("../cons");
var crypto = require("crypto");
var jwt = require('jsonwebtoken');
const passport = require('passport');
const passportConfig = require('../middleware/passport');
var { upload } = require("../middleware/multer.middleware");
var nodemailer = require('nodemailer');

router.post("/registerUser", registerUser);
router.get("/getAllUser", passport.authenticate('jwt', { session: false }), getAllUser);
router.get("/getUser", getUser);
router.post("/forgotPassword", forgotPassword);
router.post("/changePassword", changePassword);
router.patch("/updateUser", updateUser);
router.delete("/deleteUser", deleteUser);
router.post("/login", login);
router.post("/uploadAvatar", upload.single('avatar'), uploadAvatar);
router.post("/sendMail", sendMail);


module.exports = router;

async function sendMail(req, res) {
    var coin = req.body.coin;
    if (!coin) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập coin"
        });
        return;
    }

    var email = req.body.email;
    if (!email) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập email"
        });
        return;
    }
    if (!cons.paterEmail.test(email)) {
        res.json({
            statuscode: 400,
            message: "cú pháp mail của bạn không hợp lệ"
        });
        return;
    }

    var data = await userModel.find({ email: email });
    if (data[0] == "" || data[0] == undefined || data[0] == null) {
        return res.json({
            error: true,
            message: "email Không tồn tại !"
        })
    }
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'nolv3noljfe202x@gmail.com',
            pass: 'emdungdi1'
            // user: 'ducskt111@gmail.com',
            // pass: 'nguyenquanghieu113'
        }
    });
    var mainOptions = {
        from: 'Ứng dụng BestCV',
        to: email,
        subject: 'Giao dịch',
        text: 'Giao dịch của bạn đã được phê duyệt với số coin: +' + coin
    }

    transporter.sendMail(mainOptions, function (err, info) {
        if (err) {
            console.log(err);
            return res.json({ err: true, message: 'send mail fail' })
        } else {
            console.log('Message sent: ' + info.response);
            return res.json({ err: false, message: 'send mail success' })
        }
    });
}

async function uploadAvatar(req, res) {
    var email = req.body.email;
    if (!email) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập email"
        });
        return;
    }
    var avatar = req.file;

    if (!avatar) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập avatar"
        });
        return;
    }
    var data = await userModel.find({ email: email });
    if (data.length == 0) {
        let a = 'email Không tồn tại !';
        return a;
    }
    let a = await userModel.update({ _id: data[0]._id }, { avatar: avatar.filename });
    if (a) {
        return res.json({ error: false, data: a });
    }
    else {
        return res.json({ error: true, message: 'fail' });
    }
}

async function login(req, res) {
    var email = req.body.email;
    if (!email) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập username"
        });
        return;
    }
    if (!cons.paterEmail.test(email)) {
        res.json({
            statuscode: 400,
            message: "cú pháp mail của bạn không hợp lệ"
        });
        return;
    }

    var password = req.body.password;
    if (!password) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập password"
        });
        return;
    }

    let data = await userModel.findOne({ email: email });
    if (data) {
        var hash = crypto.createHmac('sha256', cons.keyPass)
            .update(password)
            .digest('hex');

        if (hash == data.password) {
            // var token = await jwt.sign({ email: email, role: data.idRole.name }, cons.keyToken, { algorithm: 'HS256', expiresIn: '3h' });
            var token = await jwt.sign({
                iss: 'Quang Hieu',
                sub: email,
                iat: new Date().getTime(),
                exp: new Date().setDate(new Date().getDate() + 1)
            }, cons.keyToken)
            // res.setHeader('Authorization', token);
            let data1 = await userModel.findOne({ email: email }).populate('idRole').populate('idWard');
            return res.json({ error: false, data: data1, token: token });
            // return res.json({ error: false, message: 'đăng nhập thành công' });
        } else {
            return res.json({ error: true, message: 'sai password đăng nhập' });
        }
    } else {
        return res.json({ error: true, message: 'sai email đăng nhập' });
    }
}

async function registerUser(req, res) {
    var email = req.body.email;
    if (!email) {
        res.json({
            statuscode: 400,
            message: "empty email"
        });
        return;
    }
    if (!cons.paterEmail.test(email)) {
        res.json({
            statuscode: 400,
            message: "email not exist"
        });
        return;
    }

    var password = req.body.password;
    if (!password) {
        res.json({
            statuscode: 400,
            message: "empty password"
        });
        return;
    }
    if (password.length < 4 || password.length > 15) {
        res.json({
            statuscode: 400,
            message: "password from 5 to 15 character"
        });
        return;
    }
    var idRole = req.body.idRole;
    if (!idRole) {
        res.json({
            statuscode: 400,
            message: "empty idRole"
        });
        return;
    }

    // var idWard = req.body.idWard;
    // if (!idWard) {
    //     res.json({
    //         statuscode: 400,
    //         message: "empty idWard"
    //     });
    //     return;
    // }

    let data = await userModel.find({ email: email });

    if (data.length > 0) {
        res.json({
            statusCode: 400,
            message: "email already exist"
        })
        return;
    }

    try {
        var hash = await crypto.createHmac('sha256', cons.keyPass)
            .update(password)
            .digest('hex');
    } catch (err) {
        return res.json({ error: true, message: 'hash false: ' + err });
    }

    password = hash;

    var user = new userModel({
        email: email,
        password: password,
        idRole: idRole
    });
    var token = await jwt.sign({
        iss: 'Quang Hieu',
        sub: email,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate() + 1)
    }, cons.keyToken)

    try {
        var saveUser = await user.save();
        let data = await userModel.findOne({ email: email }).populate('idRole');
        if (data) {
            return res.json({ error: false, data: data, token: token });
        }
        else {
            return res.json({ error: true, message: 'Them that bai' });
        }
    }
    catch (err) {
        return res.json({ error: true, message: 'saveUser false: ' + err });
    }

}

async function getAllUser(req, res) {
    try {
        var data = await userModel.find({});
        if (data) {
            return res.json({ error: false, data: data });
        }
        else {
            return res.json({ error: true, message: 'getAllUser false' });
        }
    } catch (err) {
        return res.json({ error: true, message: 'getAllUser false: ' + err });
    }
}

async function getUser(req, res) {
    var email = req.body.email;
    if (!email) {
        res.json({
            statuscode: 400,
            message: "empty email"
        });
        return;
    }
    try {
        var data = await userModel.find({ email: email });
        if (data != undefined && data != null && data != '') {
            return res.json({ error: false, data: data });
        }
        else {
            return res.json({ error: true, message: 'have no User' });
        }
    } catch (err) {
        return res.json({ error: true, message: 'getUser false: ' + err });
    }
}

// quên mật khẩu
async function forgotPassword(req, res) {
    var email = req.body.email;
    if (!email) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập email"
        });
        return;
    }
    if (!cons.paterEmail.test(email)) {
        res.json({
            statuscode: 400,
            message: "cú pháp mail của bạn không hợp lệ"
        });
        return;
    }

    var newPass = Math.floor(Math.random() * (999999 - 100000 + 1) - 100000);

    var data = await userModel.find({ email: email });
    if (data[0] == "" || data[0] == undefined || data[0] == null) {
        return res.json({
            error: true,
            message: "email Không tồn tại !"
        })
    }
    var transporter = nodemailer.createTransport({ // config mail server
        service: 'Gmail',
        auth: {
            user: 'ducskt111@gmail.com',
            pass: 'nguyenquanghieu113'
        }
    });
    var mainOptions = { // thiết lập đối tượng, nội dung gửi mail
        from: 'Ứng dụng BestCV',
        to: email,
        subject: 'Cấp mật khẩu',
        text: 'mật khẩu mới của bạn là : ' + newPass
    }

    transporter.sendMail(mainOptions, function (err, info) {
        if (err) {
            console.log(err);
        } else {
            console.log('Message sent: ' + info.response);
        }
    });
    var hash = crypto.createHmac('sha256', cons.keyPass)
        .update(password)
        .digest('hex');
    var password = hash;

    let a = await userModel.updateOne({ _id: data[0]._id }, { password: password });
    if (a) {
        return res.json({ error: false, message: 'thành công' });
    }
    else {
        return res.json({ error: true, message: 'that bai' });
    }
}
// change pass
async function changePassword(req, res) {
    var email = req.body.email;
    if (!email) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập email"
        });
        return;
    }
    if (!cons.paterEmail.test(email)) {
        res.json({
            statuscode: 400,
            message: "cú pháp mail của bạn không hợp lệ"
        });
        return;
    }
    var password = req.body.password;
    if (!password) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập password"
        });
        return;
    }
    var newPass1 = req.body.newPass1;
    if (!newPass1) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập pass lần 1"
        });
        return;
    }
    var newPass2 = req.body.newPass2;
    if (!newPass2) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập pass lần 2"
        });
        return;
    }

    var hash = crypto.createHmac('sha256', "ALTP")
        .update(password.toString())
        .digest('hex');

    var data1 = await userModel.find({ email: email });
    if (data1[0].password != hash) {
        return res.json({
            error: true,
            message: "loi password"
        })
    }
    if (newPass1 == newPass2) {
        var data = await userModel.find({ email: email });
        if (data.length == 0) {
            return res.json({
                error: true,
                message: "email Không tồn tại !"
            })
        }

        console.log(data);
        var hash = crypto.createHmac('sha256', "ALTP")
            .update(newPass1.toString())
            .digest('hex');

        let a = await userModel.update({ _id: data[0]._id }, { password: hash });
        return res.json({ error: false, message: 'đổi password thành công !' });
    } else {
        return res.json({ error: true, message: 'password Không khớp nhau !' });
    }
}

async function updateUser(req, res) {
    var email = req.body.email;
    var address = req.body.address;
    var phone = req.body.phone;
    var idRole = req.body.idRole;
    var idWard = req.body.idWard;
    var lat = req.body.lat;
    var long = req.body.long;

    let result = await userModel.findOne({ email: email });
    if (email == null || email == "" || email == undefined) {
        email = result.email;
    }
    if (address == null || address == "" || address == undefined) {
        address = result.address;
    }
    if (phone == null || phone == "" || phone == undefined) {
        phone = result.phone;
    }
    if (idRole == null || idRole == "" || idRole == undefined) {
        idRole = result.idRole;
    }
    if (idWard == null || idWard == "" || idWard == undefined) {
        idWard = result.idWard;
    }
    if (lat == null || lat == "" || lat == undefined) {
        lat = result.lat;
    }
    if (long == null || long == "" || long == undefined) {
        long = result.long;
    }

    let data = await userModel.updateOne({ _id: result._id }, { email: email, address: address, idRole: idRole, phone: phone, idWard: idWard, lat: lat, long: long });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa user
async function deleteUser(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await userModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}