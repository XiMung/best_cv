var router = require("express").Router();
var { submitCVModel } = require("../models/submitCVModel");
var { jobModel } = require("../models/jobModel");
var { technicalModel } = require("../models/technicalModel");
const { ObjectId } = require('mongodb');

router.post("/createSubmitCV", createSubmitCV);
router.patch("/updateSubmitCV", updateSubmitCV);
router.get("/getSubmitCV", getSubmitCV);
router.post("/getJobSubmitCV", getJobSubmitCV);

module.exports = router;

async function createSubmitCV(req, res) {
    var idTechnical = req.body.idTechnical;
    if (idTechnical == null || idTechnical == "" || idTechnical == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idTechnical"
        })
        return;
    }
    var idJob = req.body.idJob;
    if (idJob == null || idJob == "" || idJob == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idJob"
        })
        return;
    }

    var kind = new submitCVModel({
        idTechnical: idTechnical, idJob: idJob, isOpen: false, isDelete: false, isCare: false
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them submitCV that bai' });
    }
}

async function getJobSubmitCV(req, res) {
    var id = req.body.id;
    if (id.length == 0) {
        res.json({
            statusCode: 400,
            message: "loi id"
        })
        return;
    }
    var arr = [];
    var obj = {};
    await asyncForEach(id, async idJob => {
        var data = await submitCVModel.find({ idJob: idJob })
            .populate({ path: 'idTechnical', populate: { path: 'idDoctor', populate: { path: 'idUser' } } })
            .populate({ path: 'idTechnical', populate: { path: 'idCv' } })
            .populate({ path: 'idTechnical', populate: { path: 'idSpecialize' } })

        if (data) {
            let newData = data.filter(item => item.idTechnical)
            await asyncForEach(newData, async item => {
                obj = {
                    ...item.idTechnical.toObject(),
                    idSubmitCV: item._id,
                    isOpen: item.isOpen
                }
                arr.push(obj)
            })

        }
    })
    if (arr) {
        return res.json({ err: false, data: arr });
    } else {
        return res.json({ err: true, message: "fail" });
    }
}

async function getSubmitCV(req, res) {
    var data = await submitCVModel.find({});
    if (data) {
        var idJob = await jobModel.findOne({ id: data.idJob }).populate('idSpecialize').populate('idClinic');
        var idTechnical = await technicalModel.findOne({ id: data.idTechnical }).populate('idSpecialize').populate('idDoctor').populate('idCv');

        data[0].idJob = idJob;
        data[0].idTechnical = idTechnical;
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


async function updateSubmitCV(req, res) {
    var id = req.file;
    var idTechnical = req.body.idTechnical;
    var idJob = req.body.idJob;
    var isOpen = req.body.isOpen;
    var isDelete = req.body.isDelete;
    var isCare = req.body.isCare;

    let result = await submitCVModel.findOne({ _id: id });
    if (idTechnical == null || idTechnical == "" || idTechnical == undefined) {
        idTechnical = result.idTechnical;
    }
    if (idJob == null || idJob == "" || idJob == undefined) {
        idJob = result.idJob;
    }
    if (isOpen == null || isOpen == "" || isOpen == undefined) {
        isOpen = result.isOpen;
    }
    if (isDelete == null || isDelete == "" || isDelete == undefined) {
        isDelete = result.isDelete;
    }
    if (isCare == null || isCare == "" || isCare == undefined) {
        isCare = result.isCare;
    }
    let data = await submitCVModel.updateOne({ _id: id },
        { idTechnical: idTechnical, idJob: idJob, isCare: isCare, isDelete: isDelete, isOpen: isOpen });
    if (data) {
        return res.json({ err: false, data: "success" })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}
async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}