var router = require("express").Router();
var { jobModel } = require("../models/jobModel");
var { districtModel } = require("../models/districtModel");

router.post("/createJob", createJob);
router.put("/deleteJob", deleteJob);
router.patch("/updateJob", updateJob);
router.get("/getJob", getJob);
router.post("/getJobId", getJobId);
router.post("/getSalary", getSalary);
router.post("/getTypeWork", getTypeWork);
router.post("/getJobSpecialize", getJobSpecialize);
router.post("/getJobClinic", getJobClinic);
router.post("/filterAddressJob", filterAddressJob);

module.exports = router;

async function filterAddressJob(req, res) {
    var page = req.body.page;
    var limit = 20;
    var pageLoad;
    if (!page || page == 0) {
        pageLoad = 0;
    } else {
        pageLoad = limit * (page - 1);
    }

    var idDistrict = req.body.idDistrict;
    var idCity = req.body.idCity;
    if (!idCity) {
        res.json({
            statusCode: 400,
            error: true,
            message: "false idCity"
        })
        return;
    }

    if (idDistrict) {
        var data = await jobModel.find({ idDistrict: idDistrict }).populate('idSpecialize').populate({
            path: 'idClinic',
            populate: { path: 'idUser' }
        }).populate('idDistrict').limit(limit).skip(pageLoad);
        if (data) {
            return res.json({ error: false, data: data.reverse() });
        }
        else {
            return res.json({ error: true, message: 'false' });
        }
    }
    else {
        var result = [];
        var arr = [];
        var data = await jobModel.find().populate('idDistrict');
        var data1 = await districtModel.find({ idCity: idCity }).select('id');
        data1.forEach(x => {
            arr.push(String(x._id));
        })
        if (data1.length > 0) {
            data.forEach(item => {
                if (arr.includes(String(item.idDistrict)) == true) {
                    result.push(item);
                }
            })
        }
        return res.json({ error: false, data: result });
    }
}

async function createJob(req, res) {
    var address = req.body.address;
    if (address == null || address == "" || address == undefined) {
        res.json({
            statusCode: 400,
            message: "loi address"
        })
        return;
    }
    var idDistrict = req.body.idDistrict;
    if (idDistrict == null || idDistrict == "" || idDistrict == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idDistrict"
        })
        return;
    }
    var title = req.body.title;
    if (title == null || title == "" || title == undefined) {
        res.json({
            statusCode: 400,
            message: "loi title"
        })
        return;
    }
    var sex = req.body.sex;
    if (sex == null || sex == "" || sex == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idClinic"
        })
        return;
    }
    var idSpecialize = req.body.idSpecialize;
    if (idSpecialize == null || idSpecialize == "" || idSpecialize == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idSpecialize"
        })
        return;
    }
    var experience = req.body.experience;
    if (experience == null || experience == "" || experience == undefined) {
        res.json({
            statusCode: 400,
            message: "loi experience"
        })
        return;
    }

    var typeWork = req.body.typeWork;
    if (typeWork == null || typeWork == "" || typeWork == undefined) {
        res.json({
            statusCode: 400,
            message: "loi typeWork"
        })
        return;
    }
    var salary = req.body.salary;
    // if (salary == null || salary == "" || salary == undefined) {
    //     res.json({
    //         statusCode: 400,
    //         message: "loi salary"
    //     })
    //     return;
    // }
    var term = req.body.term;
    if (term == null || term == "" || term == undefined) {
        res.json({
            statusCode: 400,
            message: "loi term"
        })
        return;
    }
    var requirement = req.body.requirement;
    if (requirement == null || requirement == "" || requirement == undefined) {
        res.json({
            statusCode: 400,
            message: "loi experience"
        })
        return;
    }
    var benefit = req.body.benefit;
    if (benefit == null || benefit == "" || benefit == undefined) {
        res.json({
            statusCode: 400,
            message: "loi benefit"
        })
        return;
    }
    var idClinic = req.body.idClinic;
    if (idClinic == null || idClinic == "" || idClinic == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idClinic"
        })
        return;
    }
    var isActive = req.body.isActive;
    if (isActive == null || isActive == "" || isActive == undefined) {
        res.json({
            statusCode: 400,
            message: "loi isActive"
        })
        return;
    }

    var kind = new jobModel({
        title: title, sex: sex, idSpecialize: idSpecialize, experience: experience,
        typeWork: typeWork, salary: salary, term: term, requirement: requirement,
        benefit: benefit, idClinic: idClinic, isActive: isActive, status: false,
        address: address, idDistrict: idDistrict
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

async function getJob(req, res) {
    var data = await jobModel.find().populate('idSpecialize').populate({
        path: 'idClinic',
        populate: { path: 'idUser' }
    }).populate('idDistrict');
    if (data) {
        return res.json({ error: false, data: data.reverse() });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}

async function getJobClinic(req, res) {

    var page = req.body.page;
    var limit = 20;
    var pageLoad
    if (!page || page == 0) {
        pageLoad = 0;
    } else {
        pageLoad = limit * (page - 1);
    }

    var id = req.body.id;
    if (!id) {
        res.json({
            statusCode: 400,
            message: "loi id"
        })
        return;
    }

    var data = await jobModel.find({ idClinic: id }).populate('idSpecialize').populate({
        path: 'idClinic',
        populate: { path: 'idUser' }
    }).populate('idDistrict').limit(limit).skip(pageLoad);

    return res.json({ error: false, data: data.reverse() })
}

async function getJobSpecialize(req, res) {

    var page = req.body.page;
    var limit = 20;
    var pageLoad
    if (!page || page == 0) {
        pageLoad = 0;
    } else {
        pageLoad = limit * (page - 1);
    }

    var id = req.body.id;
    if (!id) {
        res.json({
            statusCode: 400,
            message: "loi id"
        })
        return;
    }

    var data = await jobModel.find({ idSpecialize: id, status: true }).populate('idSpecialize').populate({
        path: 'idClinic',
        populate: { path: 'idUser' }
    }).populate('idDistrict').limit(limit).skip(pageLoad);

    return res.json({ error: false, data: data.reverse() })
}

async function getTypeWork(req, res) {
    var page = req.body.page;
    var limit = 20;
    var pageLoad
    if (!page || page == 0) {
        pageLoad = 0;
    } else {
        pageLoad = limit * (page - 1);
    }

    var typeWork = req.body.typeWork;
    if (!typeWork) {
        res.json({
            statusCode: 400,
            message: "loi typeWork"
        })
        return;
    }

    var data = await jobModel.find({ typeWork: typeWork, status: true }).populate('idSpecialize').populate({
        path: 'idClinic',
        populate: { path: 'idUser' }
    }).populate('idDistrict').limit(limit).skip(pageLoad);

    return res.json({ error: false, data: data.reverse() })
}

async function getSalary(req, res) {
    // var page = req.body.page;
    // var limit = 2;
    // var pageLoad
    // if (!page || page == 0) {
    //     pageLoad = 0;
    // } else {
    //     pageLoad = limit * (page - 1);
    // }

    var from = req.body.from;
    if (!from) {
        from = 0;
    }
    var to = req.body.to;
    if (!to) {
        to = 0;
    }
    var data = await jobModel.find({ status: true }).populate('idSpecialize').populate({
        path: 'idClinic',
        populate: { path: 'idUser' }
    }).populate('idDistrict');

    var result = [];
    data.forEach(item => {
        if (item.salary.from >= from && item.salary.to <= to) {
            result.push(item);
        }
    });

    return res.json({ error: false, data: result.reverse() })
}

async function getJobId(req, res) {
    // var page = req.body.page;
    // var limit = 2;
    // var pageLoad
    // if (!page || page == 0) {
    //     pageLoad = 0;
    // } else {
    //     pageLoad = limit * (page - 1);
    // }
    // var data = await jobModel.find().populate('idSpecialize').populate('idClinic').limit(limit).skip(pageLoad);
    var id = req.body.id;
    if (!id) {
        res.json({
            statusCode: 400,
            message: "if fail"
        })
        return;
    }
    var data = await jobModel.find({ _id: id }).populate('idSpecialize').populate({
        path: 'idClinic',
        populate: { path: 'idUser' }
    }).populate('idDistrict');
    if (data) {
        return res.json({ error: false, data: data[0] });
    }
    else {
        return res.json({ error: true, message: 'fail' });
    }
};


// // sua city
async function updateJob(req, res) {
    var id = req.body.id;
    if (!id) {
        res.json({
            statusCode: 400,
            message: "if fail"
        })
        return;
    }

    var title = req.body.title;
    var sex = req.body.sex;
    var idSpecialize = req.body.idSpecialize;
    var experience = req.body.experience;

    var typeWork = req.body.typeWork;
    var salary = req.body.salary;
    var term = req.body.term;
    var requirement = req.body.requirement;

    var benefit = req.body.benefit;
    var idClinic = req.body.idClinic;
    var isActive = req.body.isActive;
    var status = req.body.status;

    var address = req.body.address;
    var idDistrict = req.body.idDistrict;

    let result = await jobModel.findOne({ _id: id });
    if (address == null || address == "" || address == undefined) {
        address = result.address;
    }
    if (idDistrict == null || idDistrict == "" || idDistrict == undefined) {
        idDistrict = result.idDistrict;
    }
    if (title == null || title == "" || title == undefined) {
        title = result.title;
    }
    if (sex == null || sex == "" || sex == undefined) {
        sex = result.sex;
    }
    if (idSpecialize == null || idSpecialize == "" || idSpecialize == undefined) {
        idSpecialize = result.idSpecialize;
    }
    if (experience == null || experience == "" || experience == undefined) {
        experience = result.experience;
    }

    if (typeWork == null || typeWork == "" || typeWork == undefined) {
        typeWork = result.typeWork;
    }
    if (salary == null || salary == "" || salary == undefined) {
        salary = result.salary;
    }
    if (term == null || term == "" || term == undefined) {
        term = result.term;
    }
    if (requirement == null || requirement == "" || requirement == undefined) {
        requirement = result.requirement;
    }

    if (benefit == null || benefit == "" || benefit == undefined) {
        benefit = result.benefit;
    }
    if (idClinic == null || idClinic == "" || idClinic == undefined) {
        idClinic = result.idClinic;
    }
    if (isActive == null || isActive == "" || isActive == undefined) {
        isActive = result.isActive;
    }
    if (status == null || status == "" || status == undefined) {
        status = result.status;
    }

    let data = await jobModel.updateOne({ _id: id },
        {
            title: title, sex: sex, idSpecialize: idSpecialize, experience: experience,
            typeWork: typeWork, salary: salary, term: term, requirement: requirement,
            benefit: benefit, idClinic: idClinic, isActive: isActive, status: status,
            address: address, idDistrict: idDistrict
        });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa city
async function deleteJob(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await jobModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}