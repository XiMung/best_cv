var router = require("express").Router();
var { technicalModel } = require("../models/technicalModel");
var { districtModel } = require("../models/districtModel");

router.post("/createTechnical", createTechnical);
router.put("/deleteTechnical", deleteTechnical);
// router.patch("/updateTechnical", updateTechnical);
router.get("/getTechnical", getTechnical);
router.post("/getCVSpecialize", getCVSpecialize);
router.post("/getCVIdDoctor", getCVIdDoctor);
router.post("/filter_SexCV", filter_SexCV);
router.post("/filterAddressCV", filterAddressCV);


module.exports = router;

async function getCVIdDoctor(req, res) {
    var page = req.body.page;
    var limit = 20;
    var pageLoad
    if (!page || page == 0) {
        pageLoad = 0;
    } else {
        pageLoad = limit * (page - 1);
    }

    var idDoctor = req.body.idDoctor;
    if (!idDoctor) {
        res.json({
            statusCode: 400,
            message: "loi idDoctor"
        })
        return;
    }

    var data = await technicalModel.find({ idDoctor: idDoctor }).populate('idSpecialize').populate('idCv').populate({ path: 'idDoctor', populate: { path: 'idUser' } }).populate('idDistrict').limit(limit).skip(pageLoad);

    return res.json({ error: false, data: data.reverse() })
}

async function filterAddressCV(req, res) {
    var page = req.body.page;
    var limit = 20;
    var pageLoad;
    if (!page || page == 0) {
        pageLoad = 0;
    } else {
        pageLoad = limit * (page - 1);
    }

    var idDistrict = req.body.idDistrict;
    var idCity = req.body.idCity;
    if (!idCity) {
        res.json({
            statusCode: 400,
            error: true,
            message: "false idCity"
        })
        return;
    }

    if (idDistrict) {
        var data = await technicalModel.find({ idDistrict: idDistrict }).populate('idSpecialize').populate('idCv').populate({ path: 'idDoctor', populate: { path: 'idUser' } }).populate('idDistrict').limit(limit).skip(pageLoad);
        if (data) {
            return res.json({ error: false, data: data.reverse() });
        }
        else {
            return res.json({ error: true, message: 'false' });
        }
    }
    else {
        var result = [];
        var arr = [];
        var data = await technicalModel.find().populate('idSpecialize').populate('idCv').populate({ path: 'idDoctor', populate: { path: 'idUser' } }).populate('idDistrict');
        var data1 = await districtModel.find({ idCity: idCity }).select('id');
        data1.forEach(x => {
            arr.push(String(x._id));
        })
        if (data1.length > 0) {
            data.forEach(item => {
                if (arr.includes(String(item.idDistrict)) == true) {
                    result.push(item);
                }
            })
        }
        return res.json({ error: false, data: result });
    }
}

async function filter_SexCV(req, res) {
    var page = req.body.page;
    var limit = 20;
    var pageLoad
    if (!page || page == 0) {
        pageLoad = 0;
    } else {
        pageLoad = limit * (page - 1);
    }

    var sex = req.body.sex;
    if (!sex) {
        res.json({
            statusCode: 400,
            message: "loi sex"
        })
        return;
    }
    var data = await technicalModel.find({ sex: sex }).populate('idSpecialize').populate('idDoctor').populate('idCv').limit(limit).skip(pageLoad);

    return res.json({ error: false, data: data.reverse() })
}

async function getCVSpecialize(req, res) {
    var page = req.body.page;
    var limit = 20;
    var pageLoad
    if (!page || page == 0) {
        pageLoad = 0;
    } else {
        pageLoad = limit * (page - 1);
    }

    var id = req.body.id;
    if (!id) {
        res.json({
            statusCode: 400,
            message: "loi id"
        })
        return;
    }

    var data = await technicalModel.find({ idSpecialize: id }).populate('idSpecialize').populate('idCv').populate({ path: 'idDoctor', populate: { path: 'idUser' } }).populate('idDistrict').limit(limit).skip(pageLoad);

    return res.json({ error: false, data: data.reverse() })
}

async function createTechnical(req, res) {
    var experience = req.body.experience;
    if (experience == null || experience == "" || experience == undefined) {
        res.json({
            statusCode: 400,
            message: "loi experience"
        })
        return;
    }
    var idSpecialize = req.body.idSpecialize;
    if (idSpecialize == null || idSpecialize == "" || idSpecialize == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idSpecialize"
        })
        return;
    }
    var idDoctor = req.body.idDoctor;
    if (idDoctor == null || idDoctor == "" || idDoctor == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idDoctor"
        })
        return;
    }
    var idCv = req.body.idCv;
    if (idCv == null || idCv == "" || idCv == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idCv"
        })
        return;
    }
    var certificate = req.body.certificate;
    if (certificate == null || certificate == "" || certificate == undefined) {
        res.json({
            statusCode: 400,
            message: "loi certificate"
        })
        return;
    }

    var kind = new technicalModel({
        experience: experience,
        idSpecialize: idSpecialize,
        idDoctor: idDoctor,
        idCv: idCv,
        certificate: certificate,
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

// lấy tất cả city
async function getTechnical(req, res) {
    var data = await technicalModel.find({}).populate('idCv').populate({
        path: 'idDoctor',
        populate: { path: 'idUser' }
    }).populate('idSpecialize');
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


// // // sua city
// async function updateTechnical(req, res) {
//     var id = req.body.id;
//     var experience = req.body.experience;
//     var idSpecialize = req.body.idSpecialize;
//     var idDoctor = req.body.idDoctor;
//     var idCv = req.body.idCv;
//     var certificate = req.body.certificate;

//     let result = await technicalModel.findOne({ _id: id });
//     if (experience == null || experience == "" || experience == undefined) {
//         experience = result.experience;
//     }
//     if (idSpecialize == null || idSpecialize == "" || idSpecialize == undefined) {
//         idSpecialize = result.idSpecialize;
//     }
//     if (idDoctor == null || idDoctor == "" || idDoctor == undefined) {
//         idDoctor = result.idDoctor;
//     }
//     if (idCv == null || idCv == "" || idCv == undefined) {
//         idCv = result.idCv;
//     }
//     if (certificate == null || certificate == "" || certificate == undefined) {
//         certificate = result.certificate;
//     }
//     let data = await technicalModel.updateOne({ _id: id },
//         { experience: experience, idSpecialize: idSpecialize, idDoctor: idDoctor, idCv: idCv, certificate: certificate, });
//     if (data) {
//         return res.json({ err: false, data: data })
//     } else {
//         return res.json({ err: true, message: "loi update" })
//     }
// }

// xoa city
async function deleteTechnical(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await technicalModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}