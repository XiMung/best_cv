var router = require("express").Router();
var { doctorModel } = require("../models/doctorModel");

router.post("/createDoctor", createDoctor);
router.delete("/deleteDoctor", deleteDoctor);
router.patch("/updateDoctor", updateDoctor);
router.get("/getDoctor", getDoctor);
router.post("/getDoctorId", getDoctorId);

module.exports = router;

async function getDoctorId(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi id"
        })
        return;
    }
    var data = await doctorModel.find({ idUser: id }).populate("idUser");
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}

async function createDoctor(req, res) {
    var firstName = req.body.firstName;
    if (firstName == null || firstName == "" || firstName == undefined) {
        res.json({
            statusCode: 400,
            message: "loi firstName"
        })
        return;
    }

    var lastName = req.body.lastName;
    if (lastName == null || lastName == "" || lastName == undefined) {
        res.json({
            statusCode: 400,
            message: "loi lastName"
        })
        return;
    }

    var idUser = req.body.idUser;
    if (idUser == null || idUser == "" || idUser == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idUser"
        })
        return;
    }

    var kind = new doctorModel({
        firstName: firstName,
        lastName: lastName,
        idUser: idUser
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

async function getDoctor(req, res) {
    var data = await doctorModel.find({}).populate("idUser");
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


async function updateDoctor(req, res) {
    var id = req.body.id;
    var firstName = req.body.firstName;
    var lastName = req.body.lastName;

    let result = await doctorModel.findOne({ _id: id });
    if (firstName == null || firstName == "" || firstName == undefined) {
        firstName = result.firstName;
    }
    if (lastName == null || lastName == "" || lastName == undefined) {
        lastName = result.lastName;
    }
    let data = await doctorModel.updateOne({ _id: id },
        { firstName: firstName, lastName: lastName });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

async function deleteDoctor(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await doctorModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}