var router = require("express").Router();
var { clinicModel } = require("../models/clinicModel");
var { upload } = require("../middleware/multer.middleware");

router.post("/createClinic", upload.single('image'), createClinic);
router.delete("/deleteClinic", deleteClinic);
router.patch("/updateClinic", upload.single('image'), updateClinic);
router.get("/getClinic", getClinic);
router.post("/getClinicId", getClinicId);
router.post("/getClinicIdUser", getClinicIdUser);
router.patch("/updateCoin", updateCoin);

module.exports = router;

async function updateCoin(req, res) {
    var id = req.body.id;
    var coin = req.body.coin;
    try {
        var h = await clinicModel.findOne({ _id: id });
        // console.log(h);
        var c = h.coin;
        coin = Number(c) + Number(coin);
        console.log(coin);
        var data = await clinicModel.updateOne({ _id: id },
            { coin: Number(coin) });
        if (data) {
            return res.json({ err: false, data: data })
        } else {
            return res.json({ err: true, message: "loi update" })
        }
    } catch (error) {
        return res.json({ err: true, message: "loi update" })
    }

}

async function getClinicIdUser(req, res) {
    var idUser = req.body.idUser;
    if (idUser == null || idUser == "" || idUser == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idUser"
        })
        return;
    }
    var data = await clinicModel.find({ idUser: idUser }).populate({ path: 'idUser', populate: { path: 'idRole' } });
    if (data) {
        return res.json({ error: false, data: data[0] });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}

async function getClinicId(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi id"
        })
        return;
    }
    var data = await clinicModel.find({ _id: id }).populate({ path: 'idUser', populate: { path: 'idRole' } });
    if (data) {
        return res.json({ error: false, data: data[0] });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}

async function createClinic(req, res) {
    var image = req.file;
    if (image == null || image == "" || image == undefined) {
        res.json({
            statusCode: 400,
            message: "loi image"
        })
        return;
    }

    var name = req.body.name;
    if (name == null || name == "" || name == undefined) {
        res.json({
            statusCode: 400,
            message: "loi firstName"
        })
        return;
    }

    var website = req.body.website;

    var idUser = req.body.idUser;
    if (idUser == null || idUser == "" || idUser == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idUser"
        })
        return;
    }

    var video = req.body.video;

    var description = req.body.description;

    var scale = req.body.scale;

    // var image = req.body.image;

    var kind = new clinicModel({
        name: name,
        website: website,
        idUser: idUser,
        video: video,
        coin: 0,
        description: description,
        scale: scale,
        image: image.filename
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

async function getClinic(req, res) {
    var data = await clinicModel.find({}).populate("idUser");
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


async function updateClinic(req, res) {
    var id = req.body.id;
    var name = req.body.name;
    var website = req.body.website;
    var video = req.body.video;
    var coin = req.body.coin;
    var description = req.body.description;
    var scale = req.body.scale;
    var image = req.file;

    let result = await clinicModel.findOne({ _id: id });
    if (name == null || name == "" || name == undefined) {
        name = result.name;
    }
    if (website == null || website == "" || website == undefined) {
        website = result.website;
    }
    if (video == null || video == "" || video == undefined) {
        video = result.video;
    }
    if (coin == null || coin == "" || coin == undefined) {
        coin = result.coin;
    }
    if (description == null || description == "" || description == undefined) {
        description = result.description;
    }
    if (scale == null || scale == "" || scale == undefined) {
        scale = result.scale;
    }
    if (image == null || image == "" || image == undefined) {
        image = { filename: result.image };
    }
    let data = await clinicModel.updateOne({ _id: id },
        { name: name, website: website, video: video, coin: coin, description: description, scale: scale, image: image.filename });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

async function deleteClinic(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await clinicModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}