var router = require("express").Router();
var { cvModel } = require("../models/cvModel");
var { technicalModel } = require("../models/technicalModel");
var { upload } = require("../middleware/multer.middleware");

router.post("/createCV", upload.single('avatar'), createCV);
router.delete("/deleteCV", deleteCV);
router.patch("/updateCV", updateCV);
// router.get("/getCV", getCV);

module.exports = router;

// them city
async function createCV(req, res) {
    var avatar = req.file;
    if (avatar == null || avatar == "" || avatar == undefined) {
        res.json({
            statusCode: 400,
            message: "loi avatar"
        })
        return;
    }
    var skill = req.body.skill;
    var aims = req.body.aims;
    var information = req.body.information;
    var sex = req.body.sex;
    if (sex == null || sex == "" || sex == undefined) {
        res.json({
            statusCode: 400,
            message: "loi sex"
        })
        return;
    }
    var address = req.body.address;
    if (address == null || address == "" || address == undefined) {
        res.json({
            statusCode: 400,
            message: "loi address"
        })
        return;
    }

    var idDistrict = req.body.idDistrict;
    if (idDistrict == null || idDistrict == "" || idDistrict == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idDistrict"
        })
        return;
    }

    var experience = req.body.experience;
    if (experience == null || experience == "" || experience == undefined) {
        res.json({
            statusCode: 400,
            message: "loi experience"
        })
        return;
    }
    var idSpecialize = req.body.idSpecialize;
    if (idSpecialize == null || idSpecialize == "" || idSpecialize == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idSpecialize"
        })
        return;
    }
    var idDoctor = req.body.idDoctor;
    if (idDoctor == null || idDoctor == "" || idDoctor == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idDoctor"
        })
        return;
    }
    var certificate = req.body.certificate;
    if (certificate == null || certificate == "" || certificate == undefined) {
        res.json({
            statusCode: 400,
            message: "loi certificate"
        })
        return;
    }

    var kind = new cvModel({
        avatar: avatar.filename, skill: skill, aims: aims, information: information
    });
    var saveKind = await kind.save();
    if (saveKind) {
        var kind1 = new technicalModel({
            experience: experience,
            idSpecialize: idSpecialize,
            idDoctor: idDoctor,
            idCv: saveKind._id,
            certificate: certificate,
            sex: sex,
            idDistrict: idDistrict,
            address: address
        });
        var saveKind1 = await kind1.save();
        if (saveKind1) {
            return res.json({ error: false, data: "success" });
        } else {
            return res.json({ error: true, message: 'Them technical that bai' });
        }
    }
    else {
        return res.json({ error: true, message: 'Them CV that bai' });
    }
}


// async function getCV(req, res) {
//     var data = await cvModel.find({});
//     if (data) {
//         return res.json({ error: false, data: data });
//     }
//     else {
//         return res.json({ error: true, message: 'hehe' });
//     }
// }


// // sua city
async function updateCV(req, res) {
    var id = req.body.id;
    // var avatar = req.body.avatar;
    var skill = req.body.skill;
    var aims = req.body.aims;
    var information = req.body.information;

    var result = await cvModel.findOne({ _id: id });

    if (skill == null || skill == "" || skill == undefined) {
        skill = result.skill;
    }
    if (aims == null || aims == "" || aims == undefined) {
        aims = result.aims;
    }
    if (information == null || information == "" || information == undefined) {
        information = result.information;
    }
    var data = await cvModel.updateOne({ _id: id },
        { skill: skill, aims: aims, information: information });
    // if (data) {
    //     return res.json({ err: false, data: data })
    // } else {
    //     return res.json({ err: true, message: "loi update" })
    // }

    var idTechnical = req.body.idTechnical;
    var experience = req.body.experience;
    var idSpecialize = req.body.idSpecialize;
    var idDoctor = req.body.idDoctor;
    var idCv = req.body.idCv;
    var certificate = req.body.certificate;
    var sex = req.body.sex;
    var idDistrict = req.body.idDistrict;
    var address = req.body.address;

    var result = await technicalModel.findOne({ _id: idTechnical });
    if (sex == null || sex == "" || sex == undefined) {
        sex = result.sex;
    }
    if (experience == null || experience == "" || experience == undefined) {
        experience = result.experience;
    }
    if (idSpecialize == null || idSpecialize == "" || idSpecialize == undefined) {
        idSpecialize = result.idSpecialize;
    }
    if (idDoctor == null || idDoctor == "" || idDoctor == undefined) {
        idDoctor = result.idDoctor;
    }
    if (idCv == null || idCv == "" || idCv == undefined) {
        idCv = result.idCv;
    }
    if (certificate == null || certificate == "" || certificate == undefined) {
        certificate = result.certificate;
    }
    if (idDistrict == null || idDistrict == "" || idDistrict == undefined) {
        idDistrict = result.idDistrict;
    }
    if (address == null || address == "" || address == undefined) {
        address = result.address;
    }

    var data = await technicalModel.updateOne({ _id: idTechnical },
        { experience: experience, idSpecialize: idSpecialize, idDoctor: idDoctor, idCv: idCv, certificate: certificate, sex: sex, idDistrict: idDistrict, address: address });
    if (data) {
        return res.json({ err: false, message: "success" })
    } else {
        return res.json({ err: true, message: "false" })
    }
}

// xoa city
async function deleteCV(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await cvModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}