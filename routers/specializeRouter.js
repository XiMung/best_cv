var router = require("express").Router();
var { specializeModel } = require("../models/specializeModel");

router.post("/createSpecialize", createSpecialize);
router.delete("/deleteSpecialize", deleteSpecialize);
router.patch("/updateSpecialize", updateSpecialize);
router.get("/getSpecialize", getSpecialize);

module.exports = router;

async function createSpecialize(req, res) {
    var name = req.body.name;
    if (name == null || name == "" || name == undefined) {
        res.json({
            statusCode: 400,
            message: "loi name"
        })
        return;
    }

    var kind = new specializeModel({
        name: name
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

// lấy tất cả city
async function getSpecialize(req, res) {
    var data = await specializeModel.find({});
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


// // sua city
async function updateSpecialize(req, res) {
    var id = req.body.id;
    var name = req.body.name;

    let result = await specializeModel.findOne({ _id: id });
    if (name == null || name == "" || name == undefined) {
        name = result.name;
    }
    let data = await specializeModel.updateOne({ _id: id },
        { name: name });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa city
async function deleteSpecialize(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await specializeModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}