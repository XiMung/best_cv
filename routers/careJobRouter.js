var router = require("express").Router();
var { careJobModel } = require("../models/careJobModel");

router.post("/createCareJob", createCareJob);
router.delete("/deleteCareJob", deleteCareJob);
router.patch("/updateCareJob", updateCareJob);
router.get("/getCareJob", getCareJob);

module.exports = router;

async function createCareJob(req, res) {
    var idDoctor = req.body.idDoctor;
    if (idDoctor == null || idDoctor == "" || idDoctor == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idDoctor"
        })
        return;
    }
    var idJob = req.body.idJob;
    if (idJob == null || idJob == "" || idJob == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idJob"
        })
        return;
    }

    var kind = new careJobModel({
        idDoctor: idDoctor, idJob: idJob
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

async function getCareJob(req, res) {
    var data = await careJobModel.find({});
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


async function updateCareJob(req, res) {
    var id = req.file;
    var idDoctor = req.body.avatar;
    var idJob = req.body.skill;

    let result = await careJobModel.findOne({ _id: id });

    if (idDoctor == null || idDoctor == "" || idDoctor == undefined) {
        idDoctor = result.idDoctor;
    }

    if (idJob == null || idJob == "" || idJob == undefined) {
        idJob = result.idJob;
    }

    let data = await careJobModel.updateOne({ _id: id },
        { idDoctor: idDoctor, idJob: idJob });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

async function deleteCareJob(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await careJobModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}