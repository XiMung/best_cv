var router = require("express").Router();
var { roleModel } = require("../models/roleModel");

router.post("/createRole", createRole);
router.delete("/deleteRole", deleteRole);
router.patch("/updateRole", updateRole);
router.get("/getRole", getRole);

module.exports = router;

// them city
async function createRole(req, res) {
    var name = req.body.name;
    if (name == null || name == "" || name == undefined) {
        res.json({
            statusCode: 400,
            message: "loi name"
        })
        return;
    }

    var kind = new roleModel({
        name: name
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

// lấy tất cả city
async function getRole(req, res) {
    var data = await roleModel.find({});
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


// // sua city
async function updateRole(req, res) {
    var id = req.body.id;
    var name = req.body.name;

    let result = await roleModel.findOne({ _id: id });
    if (name == null || name == "" || name == undefined) {
        name = result.name;
    }
    let data = await roleModel.updateOne({ _id: id },
        { name: name });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa city
async function deleteRole(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await roleModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}