var router = require("express").Router();
var { districtModel } = require("../models/districtModel");

router.post("/createDistrict", createDistrict);
router.delete("/deleteDistrict", deleteDistrict);
router.patch("/updateDistrict", updateDistrict);
router.get("/getDistrict", getDistrict);
router.post("/getDistrictCity", getDistrictCity);

module.exports = router;

// them 
async function createDistrict(req, res) {
    var name = req.body.name;
    if (name == null || name == "" || name == undefined) {
        res.json({
            statusCode: 400,
            message: "loi name"
        })
        return;
    }
    var idCity = req.body.idCity;
    if (idCity == null || idCity == "" || idCity == undefined) {
        res.json({
            statusCode: 400,
            message: "loi name"
        })
        return;
    }

    var kind = districtModel({
        name: name, idCity: idCity
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

// lấy tất cả 
async function getDistrict(req, res) {
    var data = await districtModel.find({});
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}
// lấy district city
async function getDistrictCity(req, res) {
    var idCity = req.body.idCity;
    if (idCity == null || idCity == "" || idCity == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idCity"
        })
        return;
    }
    var data = await districtModel.find({ idCity: idCity });
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


// // sua 
async function updateDistrict(req, res) {
    var id = req.body.id;
    var name = req.body.name;
    var idCity = req.body.idCity;

    let result = await districtModel.findOne({ _id: id });
    if (name == null || name == "" || name == undefined) {
        name = result.name;
    }
    if (idCity == null || idCity == "" || idCity == undefined) {
        idCity = result.idCity;
    }
    let data = await districtModel.updateOne({ _id: id },
        { name: name, idCity: idCity });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa
async function deleteDistrict(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await districtModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}