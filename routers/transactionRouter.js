var router = require("express").Router();
var { transactionModel } = require("../models/transactionModel");

router.post("/createTransaction", createTransaction);
router.delete("/deleteTransaction", deleteTransaction);
router.patch("/updateTransaction", updateTransaction);
router.post("/getTransaction", getTransaction);
router.post("/getTransactionId", getTransactionId);

module.exports = router;

async function getTransactionId(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi id"
        })
        return;
    }
    try {
        var data = await transactionModel.findOne({ _id: id }).populate('idProduct').populate({ path: 'idClinic', populate: { path: 'iUser' } }).populate('createBy');
        if (data) {
            return res.json({ error: false, data: data });
        }
        else {
            return res.json({ error: true, message: 'hehe' });
        }
    } catch (error) {
        return res.json({ error: true, message: 'fail id' });
    }

}

async function createTransaction(req, res) {
    var idProduct = req.body.idProduct;
    if (idProduct == null || idProduct == "" || idProduct == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idProduct"
        })
        return;
    }
    var idClinic = req.body.idClinic;
    if (idClinic == null || idClinic == "" || idClinic == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idClinic"
        })
        return;
    }
    // var createBy = req.body.createBy;
    // if (createBy == null || createBy == "" || createBy == undefined) {
    //     res.json({
    //         statusCode: 400,
    //         message: "loi createBy"
    //     })
    //     return;
    // }
    var state = req.body.state;
    if (state == null || state == "" || state == undefined) {
        res.json({
            statusCode: 400,
            message: "loi state"
        })
        return;
    }

    var kind = new transactionModel({
        idProduct: idProduct, idClinic: idClinic, createBy: null, state: state,
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}


async function getTransaction(req, res) {
    var page = req.body.page;
    var limit = 10;
    var pageLoad
    if (!page || page == 0) {
        pageLoad = 0;
    } else {
        pageLoad = limit * (page - 1);
    }

    var data = await transactionModel.find({}).populate('idProduct').populate({ path: 'idClinic', populate: { path: 'idUser' } }).populate('createBy').sort({ _id: -1 }).limit(limit).skip(pageLoad);
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


async function updateTransaction(req, res) {
    var id = req.body.id;
    var idProduct = req.body.idProduct;
    var idClinic = req.body.idClinic;
    var createBy = req.body.createBy;
    var state = req.body.state;

    let result = await transactionModel.findOne({ _id: id });
    if (idProduct == null || idProduct == "" || idProduct == undefined) {
        idProduct = result.idProduct;
    }
    if (idClinic == null || idClinic == "" || idClinic == undefined) {
        idClinic = result.idClinic;
    }
    if (createBy == null || createBy == "" || createBy == undefined) {
        createBy = result.createBy;
    }
    if (state == null || state == "" || state == undefined) {
        state = result.state;
    }
    let data = await transactionModel.updateOne({ _id: id },
        { idProduct: idProduct, idClinic: idClinic, createBy: createBy, state: state, });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa city
async function deleteTransaction(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await transactionModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}