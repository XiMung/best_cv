var router = require("express").Router();
var { wardModel } = require("../models/wardModel");

router.post("/createWard", createWard);
router.delete("/deleteWard", deleteWard);
router.patch("/updateWard", updateWard);
router.get("/getWard", getWard);
router.post("/getWardDistrict", getWardDistrict);

module.exports = router;

// them 
async function createWard(req, res) {
    var name = req.body.name;
    if (name == null || name == "" || name == undefined) {
        res.json({
            statusCode: 400,
            message: "loi name"
        })
        return;
    }
    var idDistrict = req.body.idDistrict;
    if (idDistrict == null || idDistrict == "" || idDistrict == undefined) {
        res.json({
            statusCode: 400,
            message: "loi name"
        })
        return;
    }

    var kind = new wardModel({
        name: name, idDistrict: idDistrict
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}
// lấy street district
async function getWardDistrict(req, res) {
    var idDistrict = req.body.idDistrict;
    if (idDistrict == null || idDistrict == "" || idDistrict == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idDistrict"
        })
        return;
    }
    var data = await wardModel.find({ idDistrict: idDistrict });
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}
// lấy tất cả 
async function getWard(req, res) {
    var data = await wardModel.find({});
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


// // sua 
async function updateWard(req, res) {
    var id = req.body.id;
    var name = req.body.name;
    var idDistrict = req.body.idDistrict;

    let result = await userModel.findOne({ _id: id });
    if (name == null || name == "" || name == undefined) {
        name = result.name;
    }
    if (idDistrict == null || idDistrict == "" || idDistrict == undefined) {
        idDistrict = result.idDistrict;
    }
    let data = await wardModel.updateOne({ _id: id },
        { name: name, idDistrict: idDistrict });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa
async function deleteWard(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await wardModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}