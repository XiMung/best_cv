var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var userRouter = require("./routers/userRouter");
var doctorRouter = require("./routers/doctorRouter");
var roleRouter = require("./routers/roleRouter");
var cityRouter = require("./routers/cityRouter");
var clinicRouter = require("./routers/clinicRouter");
var cvRouter = require("./routers/cvRouter");
var districtRouter = require("./routers/districtRouter");
var reviewRouter = require("./routers/reviewRouter");
var specializeRouter = require("./routers/specializeRouter");
var technicalRouter = require("./routers/technicalRouter");
var wardRouter = require("./routers/wardRouter");
var spec_clinicRouter = require("./routers/spec_clinicRouter");
var jobRouter = require("./routers/jobRouter");
var transactionRouter = require("./routers/transactionRouter");
var productRouter = require("./routers/productRouter");
var careJobRouter = require("./routers/careJobRouter");
var submitCVRouter = require("./routers/submitCVRouter");
var buyCVRouter = require("./routers/buyCVRouter");

var app = express();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With,observe");
    req.header("Access-Control-Allow-Methods", "POST, PUT, PATCH, GET, OPTIONS, DELETE");
    next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/avatar', express.static(__dirname + '/public'));

app.use('/api', userRouter);
app.use('/api', doctorRouter);
app.use('/api', roleRouter);

app.use('/api', cityRouter);
app.use('/api', clinicRouter);
app.use('/api', cvRouter);

app.use('/api', technicalRouter);
app.use('/api', districtRouter);
app.use('/api', specializeRouter);

app.use('/api', reviewRouter);
app.use('/api', wardRouter);
app.use('/api', spec_clinicRouter);

app.use('/api', jobRouter);
app.use('/api', transactionRouter);
app.use('/api', productRouter);

app.use('/api', careJobRouter);
app.use('/api', submitCVRouter);
app.use('/api', buyCVRouter);

// var connect = mongoose.connect("mongodb://localhost:27017/BestCV");
var connect = mongoose.connect("mongodb+srv://leanh:anh0944164009@cluster0-fsymw.mongodb.net/BestCV?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    });


app.listen(process.env.PORT || 1998, function () {
    console.log("app start port 1998");
});